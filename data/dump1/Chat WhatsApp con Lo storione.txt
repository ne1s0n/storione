11/05/19, 15:21 - I messaggi inviati a questo gruppo sono ora protetti con la crittografia end-to-end. Tocca per maggiori info.
11/05/19, 15:21 - Hai creato il gruppo “LO STORIONE 🐟”
11/05/19, 15:25 - Nelson: Deve essere giorno, ormai: non ho ancora aperto gli occhi ma la cazzo di luce mi ha svegliato. Mi fa male tutto e non ho nessuna voglia di iniziare. Che faccio?
11/05/19, 15:27 - Alessandro Firpo: Hai scritto il messaggio alla cieca? Bravo!
11/05/19, 15:27 - Alessandro Firpo: Come fai a leggere le risposte?
11/05/19, 15:27 - Alessandro Firpo: 🤣🤣🤣
11/05/19, 15:29 - Nelson: Mi concentro fortissimo, maledico il destino e ascolto le vostre voci.
11/05/19, 15:29 - Nelson: Sento rumore d'acqua provenire da non molto lontano. Da più in basso, si direbbe.
11/05/19, 15:30 - Alessandro Firpo: Ok. Dove ti trovi?
11/05/19, 15:31 - Nelson: Sono disteso su qualcosa di estremamente scomodo, a giudicare dalla schiena che mi sta uccidendo. Mi deve aver svegliato la luce del giorno.
11/05/19, 18:21 - Simone Marini: Hai provato ad attaccarlo? Intendo ciò su cui sei disteso.
11/05/19, 19:08 - Nelson: A giudicare dall'ematoma che sento nascere sulla schiena, sì, ho provato ad attaccarlo con la spina dorsale. Ho perso.
11/05/19, 19:09 - Nelson: Una goccia d'acqua mi cade sulla mano. Un'altra sulla testa. Non sono sicuro di voler aprire gli occhi.
11/05/19, 19:20 - Simone Marini: E aprili!
11/05/19, 19:24 - Nelson: Nnnngh.
Vedo un pezzo di cielo, velato. Sta venendo a piovere.
Vedo anche dei pezzi di rovine: un lato di una parete, una colonna di marmo.
11/05/19, 19:25 - Simone Marini: Passare lingua su denti per verificarne la presenza o meno. Sfregarsi leggermente le mani per notare se sono doloranti e/o con ecchimosi. Check percezioni arti. Check movimento arti.
11/05/19, 19:25 - Simone Marini: [Poi mi fermo perché devono giocare anche gli altri.]
11/05/19, 19:28 - Nelson: [Easy, sarà per forza discontinua la partecipazione]
11/05/19, 19:27 - Nelson: Bruciando tutta la forza di volontà di un anno intero mi tiro a sedere e mi controllo.
Ho qualche ammaccatura, ma più che altro perché pare che abbia dormito su dei sassi, sul pavimento sconnesso. Ho ancora il casco in testa, e la divisa mi ha protetto.
11/05/19, 21:05 - Alessandro Firpo: Provo a levare il casco per respirare meglio e controllare stemmi o scritte su di esso e sulla divisa
11/05/19, 22:15 - Nelson: È il mio solito casco e la mia solita divisa. Cioè, la chiamo divisa per brevità. C'è l'elmo con visiera, il sottocasco, il giaccone ignifugo, il completo antifuoco e gli anfibi. Ci sarebbero anche i guanti ma mi sa che ne ho perso uno. La mia mano senza guanto è tutta nera.
Visto che mi stanno venendo dei dubbi mi tolgo la giacca e controllo, ma la scritta sulla schiena è ancora lì dove dovrebbe essere, in giallo: "Sapeurs pompiers". E sotto, più in grande: PARIS
11/05/19, 22:16 - Nelson: Intanto le gocce si trasformano piano in una pioggia leggera.
11/05/19, 22:20 - Simone Marini: Ricordo di avere un cellulare?
11/05/19, 22:20 - Simone Marini: Anche senza aprire gli occhi, non mi serve.
11/05/19, 22:20 - Simone Marini: Mi basta toccarmi le tasche con la mano non annerita
11/05/19, 22:22 - Nelson: Gli occhi li ho già aperti, con grande fatica. E per i ricordi...
CAZZO
11/05/19, 22:23 - Nelson: Ok, ok, ci sono.
11/05/19, 22:25 - Nelson: No, ricordo distintamente di averlo tolto e lasciato in caserma con i miei affetti personali quando ho iniziato il turno. E ricordo...
11/05/19, 22:25 - Nelson: IMG-20190511-WA0000-1.jpg (file allegato)
Questo
11/05/19, 22:42 - Simone Marini: Ma minchia avrò la mia radiolina del cazzo
11/05/19, 22:42 - Simone Marini: Penso
11/05/19, 22:42 - Simone Marini: Per parlare con centrale + compagni?
11/05/19, 22:50 - Nelson: Hai eliminato questo messaggio
11/05/19, 22:52 - Nelson: Sì, sì, certo. Sto un po' agitato, sto un po' confuso. Certo. Il casco ha dentro un sistema di radiotelefonia. Lo indosso e accendo.
"Ehi, sono Marcelle. Ehi."
"Etienne mi senti? Charles?"
"Ragazzi?"
Faccio passare le quattro frequenze di servizio.
"Qui Marcel Chaucer, passo."
"Qui Marcel Chaucer, c'è qualcuno in ascolto?"
"Parla Marcel Chaucer, matricola 98615, BSPP, passo."
"C'è qualcuno?"
"C'è qualcuno?"
"Pronto."
"Qui Marcel."
"Rispondete. Eddai."
"Eddai."
La batteria della radio passa da 5 tacche a 4 tacche. La pioggia ha preso intensità: sto cominciando a bagnarmi.
11/05/19, 22:53 - Simone Marini: Rotolo verso una copertura
11/05/19, 22:53 - Simone Marini: Tettoia, qualunque cosa
11/05/19, 22:53 - Simone Marini: Basta pioggia, ha rotto il belino
11/05/19, 22:54 - Hai cambiato la descrizione del gruppo
11/05/19, 22:57 - Nelson: Giusto, giusto. Mi alzo in piedi con un grugnito e cerco riparo. Sto in mezzo a delle rovine, mi caccio sotto un costone di muro: c'è un pezzo di scala che sale verso il vuoto, lì sotto si sta all'asciutto. 
Respiro.
13/05/19, 11:06 - Nelson: [Update info: Stato di salute: molto buono, un po' bagnato]
11/05/19, 23:03 - Simone Marini: "Stato di salute: molto buono" il cazzo, penso. "Stato mentale: ammerda"
11/05/19, 23:04 - Nelson: Giusto, giusto.
11/05/19, 23:04 - Simone Marini: [Ok è molto figo e mi trattengo ora, anche se vorrei fare 2000 altri commenti, perché se no gli altri devono leggersi un mare di roba. È molto coinvolgente. Daje!]
11/05/19, 23:04 - Hai cambiato la descrizione del gruppo
11/05/19, 23:05 - ‎Simone Marini ha cambiato l'immagine del gruppo
11/05/19, 23:05 - Nelson: [ ❤ ]
11/05/19, 23:09 - Alessandro Firpo: Ripercorrono a ritroso gli eventi che ricordo e che mi hanno portato in questa situazione
11/05/19, 23:21 - Nelson: Dunque. Pensare. Pensare a ritroso. Uno degli ultimi ricordi che ho è la voce di Myriam, la mia collega. Gridava di ritirarsi, che c'era troppo calore e non si poteva proseguire. Stavamo nella torre sinistra, schiacciati come sardine sugli scalini.
Abbiamo iniziato a scendere. Io chiudevo la fila, l'ordine di marcia è quello.
No, non è l'ultimo ricordo che ho. Ho anche un'immagine. Il tetto. Il cazzo di tetto che brucia come un campo di fuoco. Eravamo ventuno e abbiamo iniziato a scendere come tanti scolaretti. Come tanti polli arrostiti.
E poi mi sono svegliato con la schiena a pezzi.
12/05/19, 08:46 - Daniele Nicola Cudin: Vedo altri colleghi? Altre persone? Vie di uscita?
12/05/19, 11:00 - Nelson: Giusto, giusto. Mi guardo un po' attorno.
Sono... sono nelle rovine di un grande edificio. Forse una chiesa? Ma non è Notre Dame, ne sono certo.
Le rovine sono lì da un po', è cresciuta parecchia erba tra le crepe del pavimento, attorno alle colonne, dappertutto.
Non si vede anima viva nelle vicinanze, ma ora piove parecchio, ci sta.
Dal mio punto di vista (sono rintanato sotto un pezzo di scala) vedo che fuori dalle rovine c'è un pezzo di prato, incolto, poi una strada di terra (che sta diventando di fango). Più in là vedo la sagoma di case: inizia una città. O almeno un paesone.
Le case sono... strane? Sembrano fatte di legno. Da qui però non vedo benissimo, tra distanza e pioggia.
La temperatura inizia ad abbassarsi. Non avrò freddo ancora per parecchio, però, grazie alla divisa.
12/05/19, 11:28 - Alessandro Firpo: Con il casco calato in testa e la tuta ben chiusa dovrei essere piuttosto protetto dalla pioggia, credo sia meglio avvicinarsi alle costruzioni per capire dove cazzo sono o per trovare un riparo più comodo.
12/05/19, 16:36 - Nelson: Ok. Anche perché mi sa che tra un po' comincio ad avere fame. Vado.
Sono... disortientato. Ho visto cose simili in belgio, villaggetti di pietra e legno, con le case tutte addossate. Ma qui sembra tutto più... trasandato? C'è fango dappertutto, e le case sono tutte scure. C'è forte odore di fuliggine.
Arrivo a venti passi dalla casa più vicina. È su due piani, al pianterreno c'è una portellone e in terra una scia di fanga che entra. La porta si apre ed esce un ragazzino, avrà otto anni. Ha una casacca di tela marrone e degli zoccoli di legno ai piedi. Porta una due fascine. Quando mi vede si blocca e inizia a gridare: "Aaaah! Maman!" Getta la legna a terra e corre alla scala, esterna, verso il piano di sopra.
Non faccio in tempo ad avvicinarmi prima che sia in casa, ma riesco a sentire chiaramente che dice, entrando: "Un geant!" Poi la porta si chiude con un tonfo.
13/05/19, 01:51 - Daniele Nicola Cudin: Probabilmente è meglio allontanarsi e vedere cosa succede da lontano, magari nascosto.
13/05/19, 10:59 - Nelson: Scappo indietro tra le rovine e mi acquatto dietro il primo pezzo di muro grande abbastanza da nascondermi.
Dopo un po' dalla porta dove era entrato il bambino si affaccia una donna. Non riesco a vedere i dettagli da qui, pare abbia un grembiule. Comunque questa guarda un po' in giro, poi si sfrega le mani per scaldarle e rientra.
13/05/19, 11:05 - Hai cambiato la descrizione del gruppo
13/05/19, 11:06 - Nelson: [Update info: leggero appetito]
13/05/19, 11:08 - Alessandro Firpo: Mi avvicino alla casa che mi sembra più grande e tenuta meglio, busso e mi faccio riconoscere. Sono un pompiere di Parigi cazzo! Ci sarà qualcuno disposto ad aiutarmi
13/05/19, 11:08 - Alessandro Firpo: Magari mi han visto anche in TV
13/05/19, 11:15 - Daniele Nicola Cudin: Io temo che siamo finiti un po' a spasso nel tempo o comunque qualcosa di strano è successo...insomma, dove sono finiti tutti i colleghi pompieri e la gente e le automobili...forse è meglio tornare dentro la chiesa per un ulteriore sopralluogo e magari recuperare qualcosa che valga come arma tipo un tondino o almeno un bastone.
13/05/19, 11:32 - Nelson: Sono combattuto, spaventato e disorientato.
Devo aggrapparmi alla razionalità, sono un pompiere di Parigi, cazzo.
Però dove sono tutti? E le macchine? E gli elicotteri? E LA CITTÀ?
13/05/19, 11:37 - Nelson: [Michel è combattuto, si decide tra le tre opzioni con un tiro di dado: opzione sociale (1-10), opzione guerriglia (11-20) , opzione sbrocco (21). L'opzione sbrocco diventa più probabile ad ogni tiro.
Per non banfare ho creato una dice room dove resta lo storico di tutti i tiri. La potete consultare qui: <a href="https://rolz.org/dr?room=Storione">https://rolz.org/dr?room=Storione</a>
Tiro e faccio uno screen del risultato]
13/05/19, 11:38 - Nelson: ‎IMG-20190513-WA0001.jpg (file allegato)
13/05/19, 11:39 - Daniele Nicola Cudin: Questo messaggio è stato eliminato
13/05/19, 11:40 - Nelson: Sento lo sbrocco che mi sale potente. Mi viene da piangere, mi viene da gridare, inizio a iperventilare. Poi si attiva l'addestramento da vigile del fuoco: mantenere la calma. Fare il punto della situazione. Mettermi in sicurezza.
Mi rintano nelle rovine, sradico un alberello che è cresciuto tra due piastrelle, mi nascondo.
13/05/19, 11:41 - Nelson: Il peso del legno tra le mani mi dà un po' di sicurezza. Il respiro torna normale. Lo sbrocco è stato vicino,però.
13/05/19, 11:42 - Hai cambiato la descrizione del gruppo
13/05/19, 11:46 - Daniele Nicola Cudin: Questo messaggio è stato eliminato
13/05/19, 11:57 - Daniele Nicola Cudin: Questo messaggio è stato eliminato
13/05/19, 11:59 - Daniele Nicola Cudin: Ho visto troppi film per non riconoscere che le cose qui non sono come dovrebbero essere... che sia viaggio nel tempo o crollo psicotico, servono certezze... magari se le cerco tipo nella chiesa o nella memoria, cosa stavo facendo prima di risvegliarmi qui?
13/05/19, 14:59 - Nelson: Sto sbroccando, me ne rendo conto. Torno a pensare al prima: ero nella squadra che è intervenuta per prima all'incendio di Notre Dame. Siamo saliti su una delle torri, cercavamo di arrivare alla guglia, per salvarla.
Siamo dovuti scendere, troppo calore. Tra le ultimissime cose che ricordo c'è la schiena del mio compagno, Renard, mentre scendiamo in fila indiana per le scale strettissime... poi più niente.
Cioè.
Non niente. Ho l'immagine di un segno. È un triangolo, e ogni vertice è un cerchio. Solo questa immagine, sullo sfondo nero.
La pioggia non accenna a diminuire. Comincio ad avere un po' fame.
13/05/19, 15:00 - Hai cambiato la descrizione del gruppo
13/05/19, 15:07 - Simone Marini: Busso alla porta. Quando ero bambino ricordo che seguivo un telefilm "ai confini della realtà".
13/05/19, 21:47 - Nelson: Momento, momento. 
Sto rintanato di nuovo tra le macerie. Ho un bastone. Sto in modalità guerriglia. Ora come ora NON MI FIDO. Perché dovrei tornare ad un approccio civile?
O busso alla porta pronto ad attaccare il povero cristo che mi apre? 
[@17348469764  forse hai perso il pezzo di prima, c'è stato un tiro, quindi per qualche tempo l'idea che ha preso il sopravvento è quella proposta da @393498148622 , ovvero mi nascondo, trovo un'arma, faccio un po' guerriglia. Per far cambiare idea a Michel devi argomentare un po', oppure seguire il flusso]
14/05/19, 04:02 - Simone Marini: [capito, Sorry]
13/05/19, 23:49 - Nelson: Mentre mi decido sul da farsi esploro un po' le rovine, cercando di bagnarmi il meno possibile. 
Doveva essere un grande edificio di culto. Si distingue chiaramente la struttura su tre navate, le basi delle colonne marmoree, dei pezzi di muro. Ma è tutto in rovina. In alcune parti ci sono proprio delle cataste di laterizi e materiale ligneo. In altri si vede il pavimento. Dappertutto cresce la vegetazione, a volte veri alberelli.
Oltre la struttura scorre un fiume lento, l'acqua marrone e maleodorante.
Sull'altra sponda vedo una città di case addossate e brulicanti. Edifici bassi, uno o due piani al massimo. Nonostante la pioggia un po' di vita c'è: gente appiedata, e anche un tizio a cavallo. La città non sembra esattamente moderna.
Più a valle sembra esserci un porto fluviale.
13/05/19, 23:55 - Alessandro Firpo: Provo ad osservare nel dettaglio le rovine cercando di capire se il legno è annaerito come se fosse bruciato e quindi capire un po' la geometria di questo ex luogo di culto se può essere Notre-Dame
14/05/19, 12:21 - Nelson: No, niente segni di incendio. Il (poco) legno che trovo, di travi e simili, è marcio. È difficile capire se è crollato perché era marcio o è marcito stando a terra. Però non vedo segni del fuoco.
Il complesso è (era) molto più piccolo di Notre Dame.
14/05/19, 04:03 - Simone Marini: Sono un pompiere ma ho per caso una passione per il medioevo?
14/05/19, 12:25 - Nelson: Seguo Game of Thrones, ma fondamentalmente per far contenta la mia ragazza, Laurie. E qui grossomodo finisce il mio rapporto con il medioevo.
Mi interessano molto di più i tempi più recenti. A mio avviso i giacobini avevano ragione, e terrore o no la rivoluzione andava fatta.
14/05/19, 12:26 - Hai cambiato la descrizione del gruppo
14/05/19, 12:26 - Nelson: [Status update: aggiunti Laurie, randello]
14/05/19, 12:27 - Nelson: Una chiatta lentissimamente si avvicina, seguendo la corrente. Se vado a riva del fiume quando passa di qui saremo a portata di voce.
14/05/19, 16:07 - Simone Marini: Provo a fare chiarezza mentale ed osservare. Se non confligge con la mia decisione precedente.
14/05/19, 18:17 - Nelson: È una chiatta di legno, larga tre-quattro metri e lunga sei-otto. Si sposta sfruttando la corrente del fiume, che però è limaccioso e lento. È carica di casse e barili, più un asino che - tristissimo sotto la pioggia battente - mangia da un sacchetto legato alla sua stessa testa.
A comandare la chiatta ci stanno due tizi armati di pali, uno in cima e uno in coda. Hanno delle ampie mantelle e cappellacci. Accanto a ciascuno di essi c'è una lanterna agganciata al suo supporto (le lanterne sono spente, dato che è pomeriggio, a giudicare dalla luce).
Mi sono guardato attorno a sufficienza per stabilire delle basi minime. Che considerazioni faccio?
14/05/19, 18:18 - Hai cambiato la descrizione del gruppo
14/05/19, 18:18 - Nelson: [Update status mentale: spaventato, cauto]
14/05/19, 18:20 - Nelson: [Potete agire liberamente, se lo fate partendo dallo stato mentale attuale va tutto liscio, se imponete dei grossi cambi/incoerenze me lo dovete giustificare (in maniera molto liberale, sapete che sono di manica larga)]
14/05/19, 18:23 - Daniele Nicola Cudin: Sembra proprio di essere nel passato... o chissà dove. Forse il prossimo passo è davvero quello di ottenere informazioni, non posso restare qui per sempre, ma con questi vestiti potrei dare troppo nell'occhio.
14/05/19, 18:26 - Nelson: Ok, c'è una delle parti di me che dice "yay! avventura!" e mi bombarda di scalette da film. Sono sicuro che però ci sono altre parti di me che vogliono chiudere gli occhi, mettersi le mani sulle orecchie e cominciare a gridare LALALALALALA finché non passa tutto.
14/05/19, 18:27 - Hai cambiato la descrizione del gruppo
14/05/19, 18:27 - Nelson: [Update status mentale: molto spaventato]
14/05/19, 18:30 - Simone Marini: La parte di me che dice "yay! avventura!" è completamente oscurata dall'incombente ombra del mio terrore. Ho la pelle d'oca. Probabilmente sto sognando. Oppure sono morto. Quest'ultima considerazione è quella a cui non voglio pensare.
14/05/19, 18:30 - Simone Marini: Cautamente mi avvicino alla riva. L'obiettivo è osservare meglio gli stronzi sulle chiatte, poi se è il caso provare ad interagire.
14/05/19, 18:31 - Simone Marini: Forse è meglio interagire in maglietta e mutande che con la divisa--sono meno diverse dai vestiti del passato. Ma invece nudo? No, scarto l'idea.
14/05/19, 18:32 - Nelson: Che faccio, mi spoglio?
14/05/19, 18:32 - Daniele Nicola Cudin: Respiro... mi concentro sul respiro e chiudo gli occhi... Non andrò in panico... quello non mi serve. Faccio il catalogo di quel che ho e di quello che so... e se non mi piace il risultato ricalcolo evidenziando meglio i miei punti di forza.
14/05/19, 18:33 - Simone Marini: [raga mi spiegao meglio: 1. avvicinarsi circospetto al fiume, stealth; se i tizi non sembrano pericolosi, uscire dal nascondiglio SENZA la tuta e vedere come reagiscono; se non sono impaurito, provare a parlare, se no fuga.]
14/05/19, 18:34 - Simone Marini: Temperatura?
14/05/19, 18:35 - Nelson: Ci saranno quindici gradi, niente vento, ma piove. Tra due/tre ore, quando calerà il sole, mi aspetto che la temperatura si abbassi decisamente.
14/05/19, 18:38 - Nelson: Mentre mi arrovello per fare chiarezza dentro di me mi avvicino per quanto possibile alla riva. Non voglio essere visto, comunque, quindi sto dietro ad un pezzo di muraglia, e spio attraverso una finestra (divelta).
Con una certa cautela mi tolgo il giaccone ignifugo e l'elmo (comincio a bagnarmi la testa).
14/05/19, 18:39 - Nelson: ‎IMG-20190514-WA0001.jpg (file allegato)
Sono vestito così.
14/05/19, 18:39 - Simone Marini: Ok
14/05/19, 18:40 - Simone Marini: Ripongo il giaccone e l'elmo, se riesco, non direttamente sotto la pioggia. Risvolto i pantaloni per coprire il catarifrangente giallo. Ho per caso sotto una maglietta bianca?
14/05/19, 18:41 - Nelson: Elmo e giaccone nascosti. Sotto ho una maglietta bianca (tipo polo, per intenderci). Sotto ancora ho la maglia della salute. Sotto i pantaloni ho dei boxer di spongebob (regalo di Laurie). Nelle scarpe ho delle calze d'ordinanza, bianche.
14/05/19, 18:42 - Simone Marini: Ok ma i pantaloni li tengo se la striscia gialla è ben nascosta col risvoltino
14/05/19, 18:42 - Simone Marini: obiettivo: mediare tra "vestiti troppo strani" e "troppo poco vestito"
14/05/19, 18:43 - Nelson: Faccio il meglio che riesco: nascondo la striscia catarifrangente, sto in maglietta.
14/05/19, 18:42 - Nelson: La chiatta intanto si avvicina: tra un momento sarà al punto più vicino rispetto alla mia posizione (per raggiungerla, eventualmente, devo comunque fare un pezzetto di argine del fiume).
È una chiatta di legno, un po' sgarrupata dall'uso. Il conducente è un uomo magro con due grossi baffi. È coperto quasi del tutto da una mantella.
14/05/19, 18:43 - Simone Marini: Hanno armi? Sembrano soldati? Se no, provo lentamente ad avvicinarmi.
14/05/19, 18:44 - Simone Marini: Piano mentale: se la chiatta inizia troppo ad avvicinarsi o mi senti minacciato, scappare--recuperando giacca ed elmo; se costretto a combattere--non mollo il bastone.
14/05/19, 18:44 - Nelson: Sotto la mantella (scura) potrebbe nascondere di tutto, anche un mitragliatore. Il pensiero però, per qualche motivo, mi risulta alieno.
14/05/19, 18:45 - Simone Marini: [ok, bene fin qui, ora vorrei fossero Daniele Nicola Cudin e Alessandro Firpo a continuare e decidere SE approcciare e come]
14/05/19, 18:48 - Nelson: Sono roso dal dubbio. È una buona occasione per prendere contatti con i locali. Allo stesso tempo non vorrei fare una cazzata. Ma è difficile che si presenti un'occasione meno pericolosa di così, con questi tizi in mezzo al fiume. Ma potrebbero spararmi (frecciarmi?)
Che faccio?
14/05/19, 18:51 - Daniele Nicola Cudin: Li sento parlare? Pagano la mia lingua?
14/05/19, 18:54 - Nelson: Non sembrano parlare in questo momento. La corrente fa il suo corso e sono impegnati a fare in modo che la chiatta non si incagli. Sono anche ai capi opposti del battello (uno a poppa, uno a prua) quindi sono piuttosto certo che se parlassero tra loro dovrebbero urlare e li sentirei.
In compenso l'asino lancia un raglio, solo parzialmente attutito dalla borsa del fieno.
14/05/19, 18:54 - Nelson: ‎[Viene allegato qui un orribile messaggio vocale contenente un raglio d'asino]
14/05/19, 18:56 - Alessandro Firpo: Provo a uscire, mi sento ridicolo per essermi tolto il giaccone, in fondo DEVE esserci una spiegazione razionale per questa situazione. Lancio un saluto sbracciandomi gridando:
14/05/19, 18:56 - Alessandro Firpo: "Ehi della barca, ciao! Mi sono perso dove siamo? Che fiume è questo?"
14/05/19, 18:57 - Alessandro Firpo: Cerco di accompagnare il parlato con la mimica
14/05/19, 19:09 - Nelson: La parte razionale di me prende il sopravvento, anche se razionalizzare significa ignorare il 95% di quello che mi dicono i sensi.
"Oi della barca!"
Il barcaiolo in cima alla chiatta alza lo sguardo, e dopo un attimo di esitazione, alza un braccio per ricambiare il saluto. Sembra più che altro colpito dal vedere una persona lì.
"Mi sono perso, dove siamo? Che fiume è questo?"
Lui subito sembra che non capisca. Gli ripeto le cose (intanto la chiatta si avvicina, la punta inizia a superarmi), parlo piano, e a un certo punto lui si illumina. Risponde delle cose che non capisco bene, una qualche forma di dialetto. Sembra cercare di rassicurarmi, ma il grosso delle cose che ripete mi sfuggono. "Sian" dice, soprattutto. "Sian, sian" quando vede che non capisco.
14/05/19, 19:12 - Alessandro Firpo: Mentre ripete Sian indica il fiume?
14/05/19, 19:13 - Nelson: Sì.
14/05/19, 19:14 - Nelson: E lo fa con una certa insistenza, come dire "oh, per forza"
14/05/19, 19:14 - Alessandro Firpo: Indico il borgo più grande e chiedo come si chiama
14/05/19, 19:15 - Alessandro Firpo: Intanto provo a pensare se sto corso d'acqua è compatibile con le dimensioni della Senna
14/05/19, 19:19 - Nelson: Gli argini sono completamente diversi, c'è un bordo molto più graduale e molto più limaccioso. Però sì, grossomodo. Potrebbe.
14/05/19, 19:18 - Nelson: Si mette a ridere e bofonchia qualcosa che non capisco. Intanto la chiatta prosegue, circa metà mi ha superato. Alla coda della chiatta c'è un altro tizio, sempre coperto da una mantella lunga. Questo è però decisamente più giovane, avrà quindici anni, forse meno. Biondino.
"Est Parisi" dice il ragazzo, con un'aria piuttosto afflitta. Ho ancora tempo per fare una domanda, prima che la chiatta si allontani.
14/05/19, 22:29 - Alessandro Firpo: Grida: Chi è al governo?... Ehm... Come si chiama il re?
14/05/19, 22:30 - Alessandro Firpo: Poi aspetta con paura la risposta
14/05/19, 23:00 - Nelson: "Luis."
Il giovane barcaiolo si toglie il cappello e lo sventola per salutare, mentre la chiatta si allontana.
"Luis le pousse."
14/05/19, 23:01 - Nelson: Posso ancora, tecnicamente, buttarmi a fiume e raggiungere la chiatta a nuoto, ma preferirei non farlo. La pioggia continua, la mia pancia inizia a brontolare.
14/05/19, 23:02 - Hai cambiato la descrizione del gruppo
14/05/19, 23:02 - Nelson: [Update info: Stato di salute: molto buono, affamato, bagnato. Stato mentale: ???]
14/05/19, 23:03 - Simone Marini: Oh, cosa da chiarire: se parlano un francese antico dovrei accorgermene, anche se sono un ignorante.
14/05/19, 23:05 - Nelson: Sì, ecco. È francese, sicuramente parlano francese. Ma le pronunce sono tutte sbagliate, e ci sono parole che ricordano il latino. Penso che, facendoci un po' l'orecchio, potrei riuscire a imparare a parlarci. Adesso come adesso però è faticoso.
14/05/19, 23:06 - Nelson: Ad esempio, quello che mi hanno detto: il re, Luis le pousse (il germoglio). Sono ignorante e alle superiori dormivo, però non me lo ricordo proprio un Luigi il germoglio
15/05/19, 06:57 - Simone Marini: Dico: scusatemi, sono uno straniero e mi sono perduto...! [Immagino di parlare come in game of thrones, che seguo DOPPIATO in francese] Vengo da diversi giorni di cammino nella selva. La madonna ha vegliato su di me, sono stato fortunato! Sapete indicarmi dove posso trovare la più vicina chiesa?
15/05/19, 06:57 - Simone Marini: Se sono nel passato, suppongo che la religione sia molto importante. Se sono morto, pure. Alè.
15/05/19, 09:51 - Nelson: Non volendo perdere l'unico contatto amichevole avuto finora, corro lungo la riva del fiume per tenere la distanza con la barca.
"Straniero" grido, "sono straniero! Aiuto! Chiesa, chiesa!"
I barcaioli adesso mi guardano entrambi, e poi assisto con una certa apprensione mentre si parlano tra loro. Il giovane sembra voler fermare la barca, accenna a puntare il palo sul fondale, ma il tizio a prua scuote il capo e addirittura alza il suo palo come per minacciare l'altro.
Il giovane a questo punto si volta e, mentre si allontana sconsolato, mi dà le ultime indicazioni: "oil, oil, ecclesia" indica una direzione "Saint Germain! Manger, laboris"
Poi, su impulso del barcaiolo anziano, la chiatta guadagna il centro del fiume, acquista velocità e si allontana del tutto.
15/05/19, 15:11 - Simone Marini: [La voglia di fare metagame e guardare su internet]
15/05/19, 19:55 - Nelson: [Sentiti libero, ne avresti un vantaggio proprio piccolo :) ]
15/05/19, 20:00 - Nelson: Raccolgo le cose che avevo nascosto e mi rintano di nuovo sotto la scala. 
Ora sono bagnato, comincio ad avere decisamente fame, e la luce inizia a cambiare.
Le opzioni sono: tornare alla casa del bambino; infilarmi nel borgo; andare a cercare la chiesa; prepararmi un giaciglio qui tra le rovine; e... attraversare il fiume a nuoto? Altro?
16/05/19, 01:04 - Simone Marini: Chiesa. La carità cristiana.
16/05/19, 01:07 - Daniele Nicola Cudin: Vada per la chiesa.
16/05/19, 16:51 - Nelson: E sia.
Tengo buona l'idea del non farmi notare e avvolgo l'elmo nel giaccone. Il randello me lo tengo come bastone per aiutarmi a camminare. Seguendo le indicazioni dei barcaioli, proseguo lungo l'argine (e a favore di corrente). Faccio così anche per tenermi lontano dalle case. Dopo qualche minuto trovo un ponte di legno, avvolto nella leggera nebbiolina che sale dal fiume. Continua a piovere e, camminando sulla riva del fiume, mi son parecchio infangato.
Stando alle indicazioni del barcaiolo (se le ho capite) devo attraversare. Risalgo la riva per arrivare alla cima del ponte e, all'ultimo, mi accorgo che c'è una piccola tettoia. Sotto di essa, al riparo dalla pioggia, monta la guardia un armigero, tristissimo.

L'ARMIGERO
Alto circa un metro e sessanta, barba e capelli biondi e folti, testa scoperta. È vestito con una casaccona che, su fondo azzurro, porta il disegno del giglio di francia. Si regge ad un bastone, alto quanto lui, e alla cinta porta un coltello lungo due spanne. Indossa guanti e stivali di cuoio.
È una figura tristissima. Si vede che no, montare la guardia in una giornata di pioggia non è il motivo per cui si è arruolato. Ora probabilmente non sa più bene quale fosse, il motivo, ma sicuramente non è questo.
L'armigero non mi ha ancora notato, posso decidere la linea d'azione.
16/05/19, 16:52 - Hai cambiato la descrizione del gruppo
16/05/19, 16:52 - Nelson: [Update info: Stato di salute: buono, lievemente stanco, affamato, bagnato, infangato. Stato mentale: deciso]
16/05/19, 18:07 - Simone Marini: Mi avvicino lentamente, faccio in modo che mi veda.
16/05/19, 18:15 - Nelson: Risalgo la riva del fiume quel tanto che basta per arrivare al lato del ponte. Non faccio movimenti bruschi, e mi piazzo ad un paio di metri dall'armigero, io sotto la pioggia, lui sotto la tettoia. Dopo qualche secondo lui alza gli occhi e mi guarda. Non sembra intenzionato a dirmi niente, ma posso sempre rompergli il cazzo.
È proprio un omino. Mo che lo vedo da davanti è ancora più piccolo, sarà un metro e cinquantacinque. Io lo supero di tutta una testa, abbondante.
Che faccio? Proseguo lungo il ponte? Lo attacco?
16/05/19, 18:16 - Simone Marini: Lentamente, gli dico:
16/05/19, 18:17 - Simone Marini: Buon giorno. Sono un viandante. Sto cercando una chiesa dove ripararmi.
16/05/19, 18:20 - Simone Marini: [Oh cazzo ho capito perché il bambino mi chiamava gigante.]
16/05/19, 18:25 - Nelson: L'armigero deglutisce e scandisce con la bocca le parole che gli dico. Non dà l'idea di essere il tubero più sveglio dell'orto. Borbotta sottovoce e poi mi dice qualcosa che non capisco. Allora ripete, si capisce che è una domanda. "Stranus?" dice "Stranus? Stranus?!?"
Sembra iniziare a scaldarsi.
16/05/19, 21:53 - Daniele Nicola Cudin: Ipotizzo dica: "straniero?" Per cui rispondo di conseguenza sì. Se la mia risposta non alleggerisce la situazione, mi scuso e ringrazio l'armigero mentre mi allontano velocemente.
16/05/19, 21:55 - Simone Marini: Provo a dire sinonimi meno in uso "Io sono un forestiero"
16/05/19, 21:55 - Simone Marini: "vengo da lontano"
16/05/19, 21:55 - Simone Marini: "non parlo la tua lingua"
16/05/19, 21:56 - Simone Marini: Faccio uno sforzo erculeo per rimanere calmo.
16/05/19, 21:56 - Simone Marini: Se riesco, faccio appello alla mia disperazione per suonare triste e scoraggiato, così almeno non pensa che lo minacci e capisce che ho bisogno di aiuto.
16/05/19, 22:29 - Nelson: L'armigero esita un momento e poi esce dalla tettoia, mi arriva quasi addosso. Io indietreggio e lui mi sta addosso, mi sta addosso e mi guarda negli occhi. Io ho un momento di panico, ma poi la sua faccia si illumina e mi butta addosso un sorriso di denti gialli.
"Benissimus!" Mi dà una pacca sulla spalla "Stranus, stranus, benissimus!"
Prima che io capisca bene cosa succede la guardia mi investe di parole. Non seguo proprio tutto ma il tono è positivo: sembra molto felice di avermi in città.
Io cerco di dire chiesa, chiesa, e lui fa sì con la testa e dice "oil, oil" e si avvia attraverso il ponte. Poi, vedendo che io sto fermo, torna e mi afferra per un braccio.
Mi lascio trascinare. Cinque minuti dopo mi deposita davanti ad un grande complesso ecclesiastico, sull'altra riva del fiume.
16/05/19, 22:33 - Nelson: La chiesa è un grande cantiere: la struttura principale è finita, ma la torre, una navata, e diversi edifici annessi sono in costruzione. Certo, non con le gru: impalcature di legno, badili, picconi, corde, mucchi di pietre squadrate e da squadrare. 
Sotto una tettoia di canne e pelli siedono quattro tizi, su una panchetta.
L'armigero mi dà una spinta e ride: "ah! Stranus! Tres gros! Benissimus!"
16/05/19, 22:38 - Simone Marini: ‎[Qui viene allegato un messaggio vocale di paura e paranoia che si conclude con "meglio di niente ma sento che vogliono ciularmi e sto allerta"]
16/05/19, 22:48 - Daniele Nicola Cudin: Immagino di essere di fronte alla Cattedrale di Notre Dame in costruzione e questa consapevolezza mi colpisce lo stomaco con una forza. Cerco di mantenere la calma e dissimulare lo sgomento mentre assecondo l'armigero che credo voglia farmi lavorare lì come operaio date la mia stazza superiore alla media dell'epoca. Al momento è necessario ottenere un pasto.
16/05/19, 22:50 - Simone Marini: ⬆⬆⬆ This.
17/05/19, 00:30 - Nelson: Inebetito e disorientato, resto fermo davanti al cantiere, mentre l'armigero avanza e inizia a parlare con i tizi sotto la tettoia.
Il tessuto urbano, da questo lato del fiume, è sensibilmente meno fitto: le case sono più rade, e attorno alla chiesa iniziano addirittura alcuni campi.
L'idea che sia Parigi... che sia Notre Dame... ma no, è impossibile: Notre Dame sta nell'ile de france - un'isola sulla senna. Lo so. Lo sanno tutti. Però questa chiesa...
17/05/19, 00:30 - Nelson: Cazzo.
17/05/19, 00:30 - Nelson: È Saint Germain
17/05/19, 00:31 - Nelson: È DAVVERO SAINT GERMAIN
17/05/19, 00:31 - Nelson: Riconosco una torre. Non il resto, ma una torre sì.
17/05/19, 00:33 - Nelson: La consapevolezza mi colpisce come un pugno, poi tutto inizia a girare, e prima di perdere conoscenza ho questo pensiero: sono nel passato. Sono davvero nel passato.
17/05/19, 00:38 - Nelson: INTERVALLO: nel sogno
17/05/19, 00:39 - Nelson: ‎IMG-20190517-WA0000.jpg (file allegato)
Vedi di muoverti, veh.
17/05/19, 00:40 - Nelson: ‎IMG-20190517-WA0001.jpg (file allegato)
Non mi piace quello che avete fatto
17/05/19, 00:41 - Nelson: ‎IMG-20190517-WA0002.jpg (file allegato)
So che non è colpa tua
17/05/19, 00:42 - Nelson: ‎IMG-20190517-WA0003.jpg (file allegato)
Ma non mi interessa
17/05/19, 00:43 - Nelson: ‎IMG-20190517-WA0004.jpg (file allegato)
Usa bene i tuoi doni
17/05/19, 00:50 - Nelson: ‎IMG-20190517-WA0005.jpg (file allegato)
E fa in fretta
17/05/19, 00:53 - Nelson: Riprendo conoscenza. Sono su un tavolo in una stanzona, probabilmente un refettorio. Sono circondato da un gruppetto di persone, quasi tutti uomini. Alcuni indossano abiti sgarrupati, di telaccia. Due indossano dei sai scuri, da frati. Parlottano tra loro e parlano con me. Il mio riprendermi sembra portare il buon umore nel gruppo. Tutti mi stanno toccando e ridono tra loro.
Sono stato spogliato, sono in maglietta. Mi hanno tolto gli stivali.
Che faccio?
17/05/19, 02:12 - Daniele Nicola Cudin: Questo messaggio è stato eliminato
17/05/19, 02:14 - Daniele Nicola Cudin: Sorrido di rimando alle persone che sono intorno intorno a me, ma è solo un attimo, il ricordo delle immagini che ho appena visto mi piomba addosso ancora più angosciante della consapevolezza di essere nel passato. Dissimulando a fatica il turbamento chiedo di poter pregare in privato e colgo l'occasione per riordinare le idee.  Nel farlo ringrazio e chiedo loro dei vestiti e del cibo e mi offro di lavorare per ripagare quanto ottenuto. Il lavoro fisico forse riuscirà a schiarirmi le idee e mi darà il tempo di riflettere. Magari avrò anche il tempo di osservare questa chiesa; San Germain strano l'abbia confusa con Notre Dame. A fine giornata chiederò anche un giaciglio per dormire e anche se difficilmente chiuderò occhio proverò a riprendermi da questa giornata.
17/05/19, 05:23 - Simone Marini: Maledetti
