# -*- coding: utf-8 -*-
#This script should take one chat blob (as backed up by whatsapp) and
#output a formatted html good to be posted into a blog (my blog).
#- names will be changed
#- pictures will become links (<img src="...">)
#- my voice will be kept as main text
#- other people voices will be kept as bubbles
#- a single chapter will be extracted, meaning that certain dates (real dates,
#  in chat) will trigger the day change in the story
#- general cleaning (deleted messages, typos, bad stuff)

import my_lib
import os

#------------- CONFIGURATION
chapters = [
	#chap 0
	{'start_date' : '1/1/00', 'start_time': '0:0', 'end_date' : '31/12/99', 'end_time' : '23:59', 
	'title' : 'Test', 
	'infile' : '/home/nelson/research/storione/data/test/prova.txt',
	'outfolder' : '/home/nelson/research/storione/result/prova/', 
	'img_prefix' : ''},
	
	#chap 1
	{'start_date' : '17/04/19', 'start_time': '15:23', 'end_date' : '17/05/19', 'end_time' : '00:37', 
	'title' : 'First chapter', 
	'infile' : '/home/nelson/research/storione/data/dump1/Chat WhatsApp con Lo storione.txt',
	'outfolder' : '/home/nelson/research/storione/result/day_one/', 
	'img_prefix' : 'https://fagufo.files.wordpress.com/2019/05/'},
	
	#chap 2
	{'start_date' : '17/05/19', 'start_time': '00:39', 'end_date' : '17/05/19', 'end_time' : '00:50', 
	'title' : 'First dream', 
	'infile' : '/home/nelson/research/storione/data/dump1/Chat WhatsApp con Lo storione.txt',
	'outfolder' : '/home/nelson/research/storione/result/day_one_dream/', 
	'img_prefix' : 'https://fagufo.files.wordpress.com/2019/05/'},
	
	#chap 3
	{'start_date' : '17/05/19', 'start_time': '00:52', 'end_date' : '21/05/19', 'end_time' : '17:32', 
	'title' : 'Second chapter', 
	'infile' : '/home/nelson/research/storione/data/dump3/Chat WhatsApp con Lo storione.txt',
	'outfolder' : '/home/nelson/research/storione/result/day_two/',
	'img_prefix' : 'https://fagufo.files.wordpress.com/2019/06/'},
	
	#chap 4
	{'start_date' : '21/05/19', 'start_time': '17:33', 'end_date' : '21/05/19', 'end_time' : '17:33', 
	'title' : 'Second dream', 
	'infile' : '/home/nelson/research/storione/data/dump3/Chat WhatsApp con Lo storione.txt',
	'outfolder' : '/home/nelson/research/storione/result/day_two_dream/',
	'img_prefix' : 'https://fagufo.files.wordpress.com/2019/06/'},
	
	#chap 5
	{'start_date' : '21/05/19', 'start_time': '17:34', 'end_date' : '29/05/19', 'end_time' : '15:40', 
	'title' : 'Third day', 
	'infile' : '/home/nelson/research/storione/data/dump4/Chat WhatsApp con Lo storione.txt',
	'outfolder' : '/home/nelson/research/storione/result/day_three/',
	'img_prefix' : 'https://fagufo.files.wordpress.com/2019/06/'},
	
	#chap 6
	{'start_date' : '29/05/19', 'start_time': '15:42', 'end_date' : '29/05/19', 'end_time' : '15:44', 
	'title' : 'Third dream', 
	'infile' : '/home/nelson/research/storione/data/dump4/Chat WhatsApp con Lo storione.txt',
	'outfolder' : '/home/nelson/research/storione/result/day_three_dream/',
	'img_prefix' : 'https://fagufo.files.wordpress.com/2019/06/'},

	#chap 7
	{'start_date' : '29/05/19', 'start_time': '15:48', 'end_date' : '11/06/19', 'end_time' : '12:07', 
	'title' : 'Fourth day', 
	'infile' : '/home/nelson/research/storione/data/dump4/Chat WhatsApp con Lo storione.txt',
	'outfolder' : '/home/nelson/research/storione/result/day_four/',
	'img_prefix' : 'https://fagufo.files.wordpress.com/2019/07/'},
	
	#chap 8
	{'start_date' : '11/06/19', 'start_time': '12:08', 'end_date' : '11/06/19', 'end_time' : '12:12',
	'title' : 'Fourth dream', 
	'infile' : '/home/nelson/research/storione/data/dump4/Chat WhatsApp con Lo storione.txt',
	'outfolder' : '/home/nelson/research/storione/result/day_four_dream/',
	'img_prefix' : 'https://fagufo.files.wordpress.com/2019/07/'},

	#chap 9
	{'start_date' : '11/06/19', 'start_time': '12:13', 'end_date' : '25/06/19', 'end_time' : '00:40',
	'title' : 'Fifth day', 
	'infile' : '/home/nelson/research/storione/data/dump4/Chat WhatsApp con Lo storione.txt',
	'outfolder' : '/home/nelson/research/storione/result/day_five/',
	'img_prefix' : 'https://fagufo.files.wordpress.com/2019/08/'}
]

#This section may or may not end up into a config file
chapterCurrent = 9

#Names conversions
users = [
	{'screen_name' :'Nelson', 'nick' :'GM', 'id' :'XXXXXXXXXXXXXXXXXXXXX'},
	{'screen_name' :'Alessandro Firpo', 'nick' :'the Bibs', 'id' :'393490650069'},
	{'screen_name' :'Simone Marini', 'nick' :'Dottornomade', 'id' :'17348469764'},
	{'screen_name' :'Daniele Nicola Cudin', 'nick' :'the Dans', 'id' :'393498148622'},
]

names = {
	'Nelson' : 'GM',
	'Alessandro Firpo' : 'the Bibs', #@393490650069
	'Simone Marini' : 'Dottornomade', #@17348469764
	'Daniele Nicola Cudin': 'the Dans' #@393498148622
}

#------------- SETUP
if not os.path.exists(chapters[chapterCurrent]['outfolder']):
	os.makedirs(chapters[chapterCurrent]['outfolder'])
outfile = chapters[chapterCurrent]['outfolder'] + '/index.html' 
fp_out = open(outfile, 'w')
my_lib.putHeader(fp_out)

#------------- PARSING THE CHAT FILE
blocks = []
with open(chapters[chapterCurrent]['infile']) as fp_in:
	while True:
		block = my_lib.readBlock(fp_in)
		#not a big fan of breaks, but I have not found a pythonish way
		#to do this...
		if block == None:
			break
			
		#is this block good to be printed?
		if not my_lib.keepBlock(block, chapters[chapterCurrent]):
			continue
		
		#if we get here we keep the block

		#hiding real people names
		my_lib.userNames(block, users)

		#keeping the block for future elaborations
		blocks.append(block)

#now we have only the good block in the collection

#let's mark attachment blocks and copy the attachments
blocks = my_lib.findAttachmentBlocks(blocks)
my_lib.copyAttachments(blocks, chapters[chapterCurrent])

#let's compact blocks of the same author in a single block
#(attach block will stay by themself)
blocks = my_lib.compact(blocks)

#ready to go HMTL
for block in blocks:
	#let's generate the html for the block
	my_lib.addHtml(block, users, chapters[chapterCurrent])

	#and print everything
	my_lib.printBlock(block, fp_out)

#------------- CLEANUP
fp_in.close()
my_lib.putFooter(fp_out)
fp_out.close()
