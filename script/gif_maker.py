#!/bin/python

import os

os.chdir("/home/nelson/research/storione/giflab/fist_gift")

#first we build the gif
cmd = ['gifsicle']
cmd.append('--loopcount=forever -d50 --unoptimize')
cmd.append('--output result/first.gif')

#ten flips
for i in range(0,10):
	cmd.append('data/empty.gif data/full_01.gif')
os.system(' '.join(cmd))

#tweaking the delays
cmd = ['gifsicle']
cmd.append('-b result/first.gif')
for i in range(0,10):
	d1 = str(100 - i * 10)
	d2 = str((i+1) * 10)
	f1 = str(i * 2)
	f2 = str(i * 2 + 1)

	cmd.append('-d' + d1 + ' "#' + f1 + '"')
	cmd.append('-d' + d2 + ' "#' + f2 + '"')
print ' '.join(cmd)
os.system(' '.join(cmd))
