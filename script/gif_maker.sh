#!/bin/bash
#easy script of gifsicle related gif commands and tests
cd /home/nelson/research/storione/giflab/fist_gift

#building the full gif
gifsicle  --loopcount=forever     \
	-d50 --unoptimize \
	data/empty.gif data/full_01.gif \
	data/empty.gif data/full_01.gif \
	--output result/first.gif

#tweaking the delays
gifsicle -b result/first.gif -d50 "#0" "#1" -d100 "#2" "#3"


