# -*- coding: utf-8 -*-
import re
import shutil
import os
import string

#this function reads a block of lines of chat, or returns
#empty if we are at the end of the stream
def readBlock(fp):
	#we need to read forward: the next line will tell us if the
	#current block is finished or not
	first = True
	res = None
	while True:
		bookmark = fp.tell()
		line = fp.readline()
		#End Of File: nothing to do anymore
		if not line:
			break
		
		#if we get here we can parse the line	
		line_parsed = parseLineHeader(line)
		newblock = line_parsed['date'] != None
		
		#Entering a new block for the second time -> return the completed block
		if newblock and not first:
			fp.seek(bookmark)
			break
		
		#Entering a new block for the first time: let's start taking notes
		if newblock and first:
			res = line_parsed
			first = False
			
		#This is the continuation of the current block
		if not newblock and not first:
			#adding to the payload
			res['payload'] += '\n' + line_parsed['payload']

		#This should never happen: we start in the middle of a block
		if not newblock and first:
			raise Exception('File pointer starts in the middle of a block: \n' + line)
	
	return res

#this function separates the line header from the line payload, e.g.
#11/05/19, 15:29 - Nelson: Mi concentro fortissimo, maledico il destino e ascolto le vostre voci.
#will be converted in a dictionary with date, time, user and payload as 
#fields. If a field is abstent it is returned None
def parseLineHeader(line):
	#first thing: we trim the unwanted stuff
	line = line.strip()
	
	#most conservative approach: no header
	res = {'date' : None, 'time' : None, 'user' : None, 'payload' : line}
	#let's try to understand if a header is indeed there
	p = re.compile("^([0-9]{2}/[0-9]{2}/[0-9]{2}), ([0-9]{2}:[0-9]{2}) - (.*)$")
	m = p.search(line)
	if m != None:
		res['date'] = m.group(1)
		res['time'] = m.group(2)
		res['payload'] = m.group(3)
		
		#need to further split the payload, sometimes there's a username
		#sometime there is not
		p = re.compile("(.*?): (.*)$")
		m = p.search(m.group(3))
		if m != None:
			res['user'] = m.group(1)
			res['payload'] = m.group(2)
	return res
	
#This function loops over the passed list of blocks and identifies
#those that are attachments (one or two lines payload, first line 
#is "filename (file allegato)", the second one, if present, is a caption
#The attachment blocks acquire the keys:
# 'attachment' = True
# 'payload' = caption (if present) or None
# 'filename' = the file name to be attached
# 'extension' = the file extension, for ease of use
#All non attachment blocks acquire the key:
# 'attachment' = False
def findAttachmentBlocks(blocks):
	#regex to find lines signalling that we have an attachment
	p = re.compile("(.*\..*)\(file allegato\)")
	
	#set of printable chars, for file name cleaning
	printable = set(string.printable)
	
	for block in blocks:
		m = p.search(block['payload'])
		if m == None:
			#it's a regular block
			block['attachment'] = False
		else:
			#it's an attachment block
			block['attachment'] = True
			block['filename'] = ''.join(filter(lambda x: x in printable, m.group(1))).strip()
			block['extension'] = block['filename'].split('.')[-1].lower()
			
			#do we have a caption?
			lines = block['payload'].split('\n')
			if len(lines) > 1:
				block['payload'] = '\n'.join(lines[1:])
			else:
				block['payload'] = None
			
	return(blocks)

#loops through blocks, when it finds an attachment block copies the
#corresponding file from infolder to outfolder. Destination file names
#are always lowercase
def copyAttachments(blocks, current_chapter):
	for block in blocks:
		if block['attachment']:
			src = os.path.dirname(current_chapter['infile']) + '/' + block['filename']
			dst = current_chapter['outfolder'] + '/' + block['filename'].lower()
			shutil.copyfile(src, dst)

#this function returns True if the block need to be kept. Reasons for
#not keeping the block include unwanted content and content out of 
#current chapter
def keepBlock(block, current_chapter):
	if block['payload'] == 'Hai eliminato questo messaggio':
		return False
	if block['payload'] == 'Questo messaggio è stato eliminato':
		return False
	if block['payload'] == 'Hai cambiato la descrizione del gruppo':
		return False
		
	#checking if the block is in the current chapter timespan
	after_start = compare_dates(block['date'], block['time'], current_chapter['start_date'], current_chapter['start_time']) >= 0
	before_end  = compare_dates(block['date'], block['time'], current_chapter['end_date'],   current_chapter['end_time'])   <= 0
	
	if not (after_start and before_end):
		return False
	
	#if we get here we need to keep this block
	return True

#compare dates and times in the format dd/mm/yy and hh:mm, returns
#greater than zero, zero, or lower than zero, if the first date
#is after, equal, or before the second one
def compare_dates(date1, time1, date2, time2):
	d1 = [int(x) for x in date1.split('/')]
	d2 = [int(x) for x in date2.split('/')]
	t1 = [int(x) for x in time1.split(':')]
	t2 = [int(x) for x in time2.split(':')]
	
	moment1 = t1[1] + t1[0] * 100 + d1[0] * 3000 + d1[1] * 100000 + d1[2] * 2000000
	moment2 = t2[1] + t2[0] * 100 + d2[0] * 3000 + d2[1] * 100000 + d2[2] * 2000000
	
	return (moment1 - moment2)

#each time a user is cited we wrap it in <strong> tag
def highlightUsers(block, users):
	for user in users:
		if block['payload'] is not None:
			block['payload'] = block['payload'].replace(user['nick'], '<strong>'+user['nick']+'</strong>')

def addHtml(block, users, currentChapter):
	#some styles to be added
	figcaption_style = 'style="text-align: center; font-style: italic;" '
	figure_style='style="display: block; margin-left: auto; margin-right: auto; width: 50%;" '
	tableUser_style = '''style="
		background: #25d366;
		border-style:hidden;
		border-width: 0px;
  		width: 100%; 
		text-align: left; 
		border-radius: 25px;
		margin:4px;
		border-collapse: collapse;"'''
	tableCell_style = 'style="padding-left: 25px;"'
	
	#room for result
	res = ''

	#first thing:let's hightlight the users
	highlightUsers(block, users)
		
	if block['attachment']:
		#special treatment for attachment block. Depending on extension:
		imgs_ext = ['jpg', 'jpeg', 'gif', 'png', 'bmp']
		if block['extension'] in imgs_ext:
			#the attachment is an image
			res += '<figure><img ' + figure_style + 'src="' + currentChapter['img_prefix'] + block['filename'].lower() + '" />'
			#is there a caption?
			if block['payload'] is not None:
				res += '<figcaption ' + figcaption_style + '>' + block['payload'] + '</figcaption>'
			res += '</figure>'
			
		elif block['extension'] == 'opus':
			#audio file
			res += '''
				<figure>
				<audio controls>
				<source src="''' + block['filename'] + '''" type="audio/ogg">
				<p>Il tuo browser non supporta HTML5. Eccoti un 
				<a href="''' + block['filename'] + '''">link al file audio</a> per sopperire.</p>
				</audio>'''
			#is there a caption?
			if block['payload'] is not None:
				res += '<figcaption  ' + figcaption_style + '>' + block['payload'] + '</figcaption>'
			res += '</figure>'
		else:
			#the attachment is of unknown type
			raise Exception('Unknown format for attachment: '+ block['filename'])
	else:
		#in payload newlines become <br>
		payload_br = block['payload'].replace('\n', '<br>')
		
		#GM goes in main text
		if block['user'] == 'GM':
			res += '<p>' + payload_br + '</p>'
		
		#Other stuff goes in other stuff
		if block['user'] == None:
			res += '<p>' + payload_br + '</p>'
		
		#User goes in table
		if (block['user'] != None) and (block['user'] != 'GM'):
			#row one: who said what
			res += '<table ' + tableUser_style + '><tr><th ' + tableCell_style + '>'
			res += block['user']
			res += '</th></tr>'
			#row two: what was said
			res += '<tr><td ' + tableCell_style + '>'
			res += payload_br
			res += '</td></tr></table>'
	
	#we are done
	block['html'] = res

#usernames are substituted in user and payload field of the block.
#values are taken from users list, each element a dictionary with
#screen_name, nick and id fields
def userNames(block, users):
	for user in users:
		block['payload'] = block['payload'].replace(user['screen_name'], user['nick'])
		block['payload'] = block['payload'].replace(user['id'], user['nick'])
		if not block['user'] == None:
			block['user'] = block['user'].replace(user['screen_name'], user['nick'])

def putHeader(fp):
	res = '''
	<html>
	<head>
	<style>
	
	</style>
	</head>	
	<body>
	'''
	fp.writelines(res)

def putFooter(fp):
	fp.writelines('</body></html>\n')

def printBlock(block, fp_out):
	fp_out.writelines(block['html'] + '\n')

#loops over the block list and joins those blocks that are:
#-contiguous
#-from the same user
#-not attachments
def compact(blocks):
	newBlocks = []
	after_attach = False
	
	currentUser = ''
	for block in blocks:
		if block['user'] != currentUser or block['attachment'] or after_attach:
			#new block from a new user, or attachment, or last one was
			#and attachment: let's create a new head block
			newBlocks.append(block)
			currentUser = block['user']
			after_attach = False
		else:
			#another block from the old user, let's just add the payload
			newBlocks[-1]['payload'] += '\n' + block['payload']
		
		#if the block we just did was an attachment, we need to take
		#notes for next round
		if block['attachment']:
			after_attach = True
	
	#we are done, let's return the new list
	return(newBlocks)
